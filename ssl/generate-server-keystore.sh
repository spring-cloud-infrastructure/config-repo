#!/bin/bash

# $1 -> root-ca keystore name / root-ca keystore alias
# $2 -> root-ca keystore password
# $3 -> service name / service keystore alias
# $4 -> service keystore password

rm -r ./"$3"
mkdir ./"$3"

# Signed Server Certificate
keytool -genkeypair \
  -keyalg RSA \
  -keysize 4096 \
  -alias "$3" \
  -dname "CN=$3,OU=API,O=Spring Cloud,L=Barcelona,ST=Catalonia,C=ES" \
  -ext BasicConstraints=ca:false \
  -ext ExtendedKeyUsage=serverAuth \
  -ext SubjectAlternativeName=DNS:localhost,IP:127.0.0.1,DNS:"$3",IP:10.0.0.0,IP:10.0.0.1,IP:10.0.0.2,IP:10.0.0.3,IP:10.0.0.4,IP:10.0.0.5,IP:10.0.0.6,IP:10.0.0.7,IP:10.0.0.8,IP:10.0.0.9,IP:10.0.0.10,IP:10.0.0.11,IP:10.0.0.12,IP:10.0.0.13,IP:10.0.0.14,IP:10.0.0.15,IP:10.0.0.16,IP:10.0.0.17,IP:10.0.0.18,IP:10.0.0.19,IP:10.0.0.20,IP:10.0.0.21,IP:10.0.0.22,IP:10.0.0.23,IP:10.0.0.24,IP:10.0.0.25,IP:10.0.0.26,IP:10.0.0.27,IP:10.0.0.28,IP:10.0.0.29,IP:10.0.0.30,IP:10.0.0.31,IP:10.0.0.32,IP:10.0.0.33,IP:10.0.0.34,IP:10.0.0.35,IP:10.0.0.36,IP:10.0.0.37,IP:10.0.0.38,IP:10.0.0.39,IP:10.0.0.40 \
  -validity 3650 \
  -keystore ./"$3"/keystore.p12 \
  -storepass "$4" \
  -keypass "$4"

keytool -certreq \
  -keystore ./"$3"/keystore.p12 \
  -storepass "$4" \
  -alias "$3" \
  -keypass "$4" \
  -file ./"$3"/keystore.csr

keytool -gencert \
  -keystore ./"$1"/keystore.p12 \
  -storepass "$2" \
  -infile ./"$3"/keystore.csr \
  -alias "$1" \
  -keypass "$2" \
  -ext BasicConstraints=ca:false \
  -ext ExtendedKeyUsage=serverAuth \
  -ext SubjectAlternativeName=DNS:localhost,IP:127.0.0.1,DNS:"$3",IP:10.0.0.0,IP:10.0.0.1,IP:10.0.0.2,IP:10.0.0.3,IP:10.0.0.4,IP:10.0.0.5,IP:10.0.0.6,IP:10.0.0.7,IP:10.0.0.8,IP:10.0.0.9,IP:10.0.0.10,IP:10.0.0.11,IP:10.0.0.12,IP:10.0.0.13,IP:10.0.0.14,IP:10.0.0.15,IP:10.0.0.16,IP:10.0.0.17,IP:10.0.0.18,IP:10.0.0.19,IP:10.0.0.20,IP:10.0.0.21,IP:10.0.0.22,IP:10.0.0.23,IP:10.0.0.24,IP:10.0.0.25,IP:10.0.0.26,IP:10.0.0.27,IP:10.0.0.28,IP:10.0.0.29,IP:10.0.0.30,IP:10.0.0.31,IP:10.0.0.32,IP:10.0.0.33,IP:10.0.0.34,IP:10.0.0.35,IP:10.0.0.36,IP:10.0.0.37,IP:10.0.0.38,IP:10.0.0.39,IP:10.0.0.40 \
  -validity 3650 \
  -rfc \
  -outfile ./"$3"/keystore.crt

keytool -importcert \
  -noprompt \
  -keystore ./"$3"/keystore.p12 \
  -storepass "$4" \
  -alias "$1" \
  -keypass "$4" \
  -file ./"$1"/keystore.crt
keytool -importcert \
  -noprompt \
  -keystore ./"$3"/keystore.p12 \
  -storepass "$4" \
  -alias "$3" \
  -keypass "$4" \
  -file ./"$3"/keystore.crt

rm ./"$3"/keystore.csr
