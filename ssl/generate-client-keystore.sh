#!/bin/bash

# $1 -> root-ca keystore name / root-ca keystore alias
# $2 -> root-ca keystore password
# $3 -> service name / service keystore alias
# $4 -> service keystore password

rm -r ./"$3"
mkdir ./"$3"

# Signed Server Certificate
keytool -genkeypair \
  -keyalg RSA \
  -keysize 4096 \
  -alias "$3" \
  -dname "CN=$3,OU=API,O=Spring Cloud,L=Barcelona,ST=Catalonia,C=ES" \
  -ext BasicConstraints=ca:false \
  -ext ExtendedKeyUsage=clientAuth \
  -validity 3650 \
  -keystore ./"$3"/keystore.p12 \
  -storepass "$4" \
  -keypass "$4"

keytool -certreq \
  -keystore ./"$3"/keystore.p12 \
  -storepass "$4" \
  -alias "$3" \
  -keypass "$4" \
  -file ./"$3"/keystore.csr

keytool -gencert \
  -keystore ./"$1"/keystore.p12 \
  -storepass "$2" \
  -infile ./"$3"/keystore.csr \
  -alias "$1" \
  -keypass "$2" \
  -ext BasicConstraints=ca:false \
  -ext ExtendedKeyUsage=clientAuth \
  -validity 3650 \
  -rfc \
  -outfile ./"$3"/keystore.crt

keytool -importcert \
  -noprompt \
  -keystore ./"$3"/keystore.p12 \
  -storepass "$4" \
  -alias "$1" \
  -keypass "$4" \
  -file ./"$1"/keystore.crt
keytool -importcert \
  -noprompt \
  -keystore ./"$3"/keystore.p12 \
  -storepass "$4" \
  -alias "$3" \
  -keypass "$4" \
  -file ./"$3"/keystore.crt

rm ./"$3"/keystore.csr
