#!/bin/bash

# $1 -> root-ca keystore name / root-ca keystore alias
# $2 -> root-ca keystore password

rm -r ./"$1"
mkdir ./"$1"

# Certificate for Root CA
keytool -genkeypair \
  -keyalg RSA \
  -keysize 4096 \
  -alias "$1" \
  -dname "CN=Spring Cloud Root CA,OU=Certificate Authority,O=Spring Cloud,L=Barcelona,ST=Catalonia,C=ES" \
  -ext BasicConstraints=ca:true \
  -ext KeyUsage=digitalSignature,keyCertSign,cRLSign \
  -validity 3650 \
  -keystore ./"$1"/keystore.p12 \
  -storepass "$2" \
  -keypass "$2"

keytool -exportcert \
  -keystore ./"$1"/keystore.p12 \
  -storepass "$2" \
  -alias "$1" \
  -rfc \
  -file ./"$1"/keystore.crt
