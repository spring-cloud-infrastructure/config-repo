#!/bin/bash

# $1 -> service name / truststore alias
# $2 -> truststore password
# $3 -> keystore certificate to import / keystore alias to import

keytool -importcert \
  -noprompt \
  -keystore ./"$1"/truststore.p12 \
  -storepass "$2" \
  -alias "$3" \
  -keypass "$2" \
  -file ./"$3"/keystore.crt

openssl pkcs12 \
  -in ./"$1"/truststore.p12 \
  -out ./"$1"/truststore.crt \
  -nokeys \
  -passin pass:"$2" \
  -passout pass:"$2"
