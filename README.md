# Config Repository

Configuration store repository for Spring Config Server.

## 1. [config](./config)

Externalized configuration for services, while the properties inside of [application.yml](./config/application.yml) file
can be overridden by a service-specific `yml` file.

Sample: `API Gateway` fetches [application.yml](./config/application.yml) and
applies [api-gateway/application.yml](config/api-gateway/application.yml) on the top.

## 2. [property-encryption](./property-encryption)

Java Keystore for Spring Config Server properties encryption. [generate.sh](./property-encryption/generate.sh) can be
used to regenerate the keystore.

## 3. [ssl](./ssl)

PKCS12 Keystores and Truststores for services and clients. [generate-all.sh](./ssl/generate-all.sh) can be used to
regenerate Keystores and Truststores.

#### Import Client and Certificate Authority certificates to Firefox

- Open [about:preferences](about:preferences) in Firefox
- Search for `View Certificates` button
- click on `import` under `Your Certificates` section
- Look for [./ssl/super-admin/keystore.p12](./ssl/super-admin/keystore.p12) keystore
- Use `SuperAdmin` as a keystore password
- Open `Authorities` section
- Click on `import` button
- Look for [./ssl/root-ca/keystore.crt](./ssl/root-ca/keystore.crt) certificate
- Make sure the option `This certificate can identify websites` is checked

#### Add certificates to Postman

- Open Postman
- Open `File/Settings/Certificates` window
- Import Certificate Authority certificate:
    - Click on `Select PEM file` under `CA Certificates` section
    - Look for [./ssl/root-ca/keystore.crt](./ssl/root-ca/keystore.crt) file
- Import Client certificate:
    - Click on `Add Certificate` under `Client Certificates` section
    - Fill up `Host` with `10.0.0.1` IP and `80` port
    - Click on `Select PEX file`
    - Look for [./ssl/api-user/keystore.p12](./ssl/api-user/keystore.p12) file
    - Fill up `Passphrase` with `ApiUser`
    - Click on `Add` button
