#!/usr/bin/env bash

# Create JKS certificate
keytool -genkeypair \
  -keyalg RSA \
  -alias property-encryption \
  -dname "CN=Property Encryption,OU=API,O=Spring Cloud,L=Barcelona,ST=Catalonia,C=ES" \
  -keypass PropertyEncryptionKeyStore \
  -keystore keystore.jks \
  -storepass PropertyEncryptionKeyStore
